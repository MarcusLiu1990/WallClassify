package com.mgcoco.wallclassify.view.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.PrefUtil;
import com.mgcoco.wallclassify.view.fragment.BaseFragment;
import com.mgcoco.wallclassify.view.fragment.HomeFragment;

import butterknife.ButterKnife;

/**
 * Created by MarcusLiu on 2017/12/4.
 */

public class BaseActivity extends AppCompatActivity {

    protected BaseFragment mCurrentFragment, mLastFragment;
    private int mFrameLayoutId;
    private View mLoadingView;
    private int mApiCount = 0;
    public FragmentManager mFragmentManager;

    protected void onCreate(int layoutId, int frameLayoutId) {
        mFrameLayoutId = frameLayoutId;
        onCreate(layoutId);
    }

    protected void onCreate(int layoutId) {
        setContentView(layoutId);
        ButterKnife.bind(this);
        mFragmentManager = getSupportFragmentManager();
        mLoadingView = findViewById(R.id.main_loading);
//        setStatusbarTransparent(this);
    }

    public void setStatusbarTransparent(Activity activity) {
        Window window = activity.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(Color.TRANSPARENT);

            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            window.getDecorView().setSystemUiVisibility(uiOptions);
            //without navigation bar transparent.
//            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    public void changeContent() {
        if(!isFinishing()) {
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

            try {
                if (mCurrentFragment != null && mCurrentFragment.getClass().getSimpleName().equals(HomeFragment.class.getSimpleName())) {
                    fragmentTransaction = fragmentTransaction.replace(mFrameLayoutId, mCurrentFragment, mCurrentFragment.getClass().getSimpleName());
                }
                else {
                    if (mCurrentFragment.isAdded())
                        return;

                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_right);

                    fragmentTransaction = fragmentTransaction.add(mFrameLayoutId, mCurrentFragment, mCurrentFragment.getClass().getSimpleName())
                            .addToBackStack(mCurrentFragment.getClass().getSimpleName());

                }
                fragmentTransaction.commitAllowingStateLoss();
                if(!mCurrentFragment.getClass().getSimpleName().equals(HomeFragment.class.getSimpleName()))
                    PrefUtil.setStringPreference(this, Constants.Pref.LAST_STAY_PAGE, mCurrentFragment.getClass().getSimpleName());
            }
            catch (IllegalStateException e){
                e.printStackTrace();
            }
        }
        Fresco.getImagePipeline().clearMemoryCaches();
    }

    public void popupBackFragment(){
        if(!isFinishing()) {
            mFragmentManager.popBackStack();
        }
        Fresco.getImagePipeline().clearMemoryCaches();
    }

    @Override
    public void onResume(){
        super.onResume();
//        setStatusbarTransparent(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initialLoadingView(int resource){
//        mLoadingView = findViewById(resource);
//        ImageView imageView = mLoadingView.findViewById(R.id.item_progress_image);
//        WaveDrawable waveDrawable = new WaveDrawable(this, R.drawable.icon_maze_512);
//        imageView.setImageDrawable(waveDrawable);
//        waveDrawable.setWaveSpeed(10);
//        waveDrawable.setWaveAmplitude(50);
//        waveDrawable.setWaveLength(300);
//        waveDrawable.setLevel(5000);
//        waveDrawable.setIndeterminate(true);
    }

    public void showLoadingView(){
        if(mApiCount > 0) {
            ++mApiCount;
        } else {
            mApiCount = 1;
        }

        if(mLoadingView != null && mLoadingView.getVisibility() != View.VISIBLE)
            mLoadingView.setVisibility(View.VISIBLE);
    }

    public void addApiRequest() {
        ++mApiCount;
    }

    public void removeApiRequest() {
        --mApiCount;
        if(mApiCount <= 0) {
            mApiCount = 0;
        }
        Log.e("", "URI removeApi:" + mApiCount);
        if(mApiCount == 0 && mLoadingView != null && mLoadingView.getVisibility() != View.GONE)
            mLoadingView.setVisibility(View.GONE);
    }

    public boolean isLoadingViewShow(){
        if(mLoadingView != null && mLoadingView.getVisibility() != View.GONE)
            return true;
        else
            return false;
    }
}