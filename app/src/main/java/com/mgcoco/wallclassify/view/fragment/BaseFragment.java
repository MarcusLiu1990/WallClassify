package com.mgcoco.wallclassify.view.fragment;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mgcoco.wallclassify.entity.response.ClassifyData;
import com.mgcoco.wallclassify.entity.response.LikeData;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.PrefUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by MarcusLiu on 2017/12/11.
 */

public abstract class BaseFragment extends Fragment {

    public Context mContext;
    protected View mView;
    public abstract void refreshUI();
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    public View getView(){
        return mView;
    }

    public Bitmap getViewCache(){
        mView.buildDrawingCache(true);
        return mView.getDrawingCache();
    }

    public String getLocalClassify(){
        return PrefUtil.getStringPreference(mContext, Constants.Pref.LOCAL_CLASSIFY);
    }

    public List<ClassifyData> getLocalClassifyList(){
        String localClassify = getLocalClassify();
        if(localClassify != null) {
            return new Gson().fromJson(localClassify, new TypeToken<List<ClassifyData>>() {}.getType());
        }
        return null;
    }

    public HashMap<String, LikeData> getLikeDataHash(List<LikeData> list){
        HashMap<String, LikeData> hml = new HashMap<>();
        for(LikeData ld:list) {
            if(ld.getPageId() != null)
                hml.put(ld.getPageId(), ld);
            else
                hml.put(ld.getId(), ld);
        }
        return hml;
    }

    public void saveLocalClassifyList(List<ClassifyData> data){
        PrefUtil.setStringPreference(mContext, Constants.Pref.LOCAL_CLASSIFY, new Gson().toJson(data));
    }
}