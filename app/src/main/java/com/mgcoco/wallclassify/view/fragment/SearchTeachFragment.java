package com.mgcoco.wallclassify.view.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.adapter.SearchTeachPagerAdapter;
import com.mgcoco.wallclassify.view.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by softicecan on 2018/3/18.
 */

public class SearchTeachFragment extends BaseFragment {

    @BindView(R.id.search_teach_pager)
    ViewPager mPager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_teach, container, false);
        ButterKnife.bind(this, mView);

        List<String> url = new ArrayList<>();
        url.add("http://res.cloudinary.com/savecoco/image/upload/v1521368633/1_betf8y.jpg");
        url.add("http://res.cloudinary.com/savecoco/image/upload/v1521368633/2_illfos.jpg");
        url.add("http://res.cloudinary.com/savecoco/image/upload/v1521368634/3_hfqfn4.jpg");
        url.add("http://res.cloudinary.com/savecoco/image/upload/v1521368634/4_at9sek.jpg");
        SearchTeachPagerAdapter adapter = new SearchTeachPagerAdapter(mContext, url);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)mContext).popupBackFragment();
            }
        });
        mPager.setAdapter(adapter);

        CircleIndicator indicator = (CircleIndicator) mView.findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        return mView;
    }


    @Override
    public void refreshUI() {

    }
}
