package com.mgcoco.wallclassify.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.entity.response.ClassifyData;
import com.mgcoco.wallclassify.entity.response.LikeData;
import com.mgcoco.wallclassify.manager.PopupManager;
import com.mgcoco.wallclassify.network.FireStoreRequest;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.PrefUtil;
import com.mgcoco.wallclassify.view.fragment.BaseFragment;
import com.mgcoco.wallclassify.view.fragment.ClassifyFragment;
import com.mgcoco.wallclassify.view.fragment.HomeFragment;
import com.mgcoco.wallclassify.view.fragment.PostsFragment;
import com.mgcoco.wallclassify.view.fragment.SearchTeachFragment;
import com.mgcoco.wallclassify.widget.AdFacebook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    public enum FragmentPage{
        PAGE_HOME, PAGE_CLASSIFY, PAGE_POST, PAGE_SEARCH_TEACH
    }

    @BindView(R.id.toolbar_title)
    TextView mTitleView;
    @BindView(R.id.toolbar_right)
    ImageView mRightBtn;
    @BindView(R.id.main_banner_parent)
    RelativeLayout mBannerParent;


    private String mTitle;
    private HashMap<String, ClassifyData> mClassMap = new HashMap<>();
    private List<LikeData> mLikeList = new ArrayList<>();

    private ClassifyData mCurrentClassify;

    private NativeAd mAdBannerView;
    //    private AdView mAdBannerView;
    private FirebaseFirestore mFirestore;
    private FireStoreRequest mFireStoreRequest;
    private boolean mIsRepickLike = false;
    private long mLastCreateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(R.layout.activity_main, R.id.main_framelayout);

//        mAdView = findViewById(R.id.main_adView);
//        final AdRequest adRequest = new AdRequest.Builder().build();
////        mAdView.setAdSize(AdSize.SMART_BANNER );
////        mAdView.setAdUnitId("ca-app-pub-4469629028224752/6520873952");
//        mAdView.setAdListener(new AdListener(){
//            @Override
//            public void onAdLoaded() {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(mBannerParent != null && mAdView != null)
//                            mAdView.loadAd(adRequest);
//                    }
//                }, Constants.RELOAD_DURATION);
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(mBannerParent != null && mAdView != null)
//                            mAdView.loadAd(adRequest);
//                    }
//                }, Constants.RETRY_DURATION);
//            }
//
//        });
//        mAdView.loadAd(adRequest);

//        mAdBannerView = new AdView(MainActivity.this, Constants.AD_ID_FB_HOME_BANNER, AdSize.BANNER_HEIGHT_50);
//        mAdBannerView.setAdListener(new AdListener() {
//            @Override
//            public void onError(Ad ad, AdError adError) {
//                Log.e("", "onError " + adError.getErrorMessage() + ", " + ad);
//                if(mBannerParent != null)
//                    mBannerParent.setVisibility(View.GONE);
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(mBannerParent != null && mAdBannerView != null)
//                            mAdBannerView.loadAd();
//                    }
//                }, Constants.RETRY_DURATION);
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                if(mBannerParent != null)
//                    mBannerParent.setVisibility(View.VISIBLE);
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(mBannerParent != null && mAdBannerView != null)
//                            mAdBannerView.loadAd();
//                    }
//                }, Constants.RELOAD_DURATION);
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//            }
//        });

        loadBanner();

        mUiHandler.sendEmptyMessage(1);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                mFirestore = FirebaseFirestore.getInstance();
                mFireStoreRequest = new FireStoreRequest(mFirestore);
                mUiHandler.sendEmptyMessage(0);

            }
        }).start();


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment currentFragment = getCurrentFragment();
                if(currentFragment != null) {
                    if(!currentFragment.getClass().getSimpleName().equals(HomeFragment.class.getSimpleName()))
                        PrefUtil.setStringPreference(MainActivity.this, Constants.Pref.LAST_STAY_PAGE, currentFragment.getClass().getSimpleName());
                    mCurrentFragment = (BaseFragment) currentFragment;
                    if (currentFragment.getClass().getSimpleName().equals(HomeFragment.class.getSimpleName())) {
                        mRightBtn.setVisibility(View.GONE);
                        mTitle = getString(R.string.app_name);
                        mTitleView.setText(mTitle);
                    } else if (currentFragment.getClass().getSimpleName().equals(ClassifyFragment.class.getSimpleName())) {
                        mRightBtn.setVisibility(View.GONE);
                        if (mCurrentClassify != null)
                            mTitle = mCurrentClassify.getTitle();
                        mTitleView.setText(mTitle);
                    } else if (currentFragment.getClass().getSimpleName().equals(PostsFragment.class.getSimpleName())) {
                        mRightBtn.setVisibility(View.VISIBLE);
                        mRightBtn.setImageResource(R.drawable.icon_settings);
                        if (mCurrentClassify != null)
                            mTitle = mCurrentClassify.getTitle();
                        mTitleView.setText(mTitle);
                    }
                }
            }
        });

        mLastCreateTime = System.currentTimeMillis();
    }

    private void initialUI(){

        String lastStayPage = PrefUtil.getStringPreference(this, Constants.Pref.LAST_STAY_PAGE);
        String lastClassify = PrefUtil.getStringPreference(this, Constants.Pref.LAST_STAY_CLASSIFY);
        System.out.println("initialUI:" + lastStayPage + ", " + lastClassify);

        gotoFragment(FragmentPage.PAGE_HOME);

        if(lastStayPage != null && lastStayPage.equals(PostsFragment.class.getSimpleName()) && lastClassify != null){
            ClassifyData classifyData = new Gson().fromJson(lastClassify, new TypeToken<ClassifyData>() {}.getType());
            setCurrentClassify(classifyData);
            gotoFragment(FragmentPage.PAGE_POST);
        }
        PopupManager.loadLeaveAppConfirmView(MainActivity.this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void loadBanner(){
        if(mBannerParent == null)
            return;

        AdFacebook.getAdInstance(this).loadNativeAd(mBannerParent, "1000091930154368_1019440684886159", new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e("", "onError " + adError.getErrorMessage() + ", " + ad);
                if(mBannerParent != null)
                    mBannerParent.setVisibility(View.GONE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadBanner();
                    }
                }, Constants.RETRY_DURATION);
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if(mBannerParent != null)
                    mBannerParent.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadBanner();
                    }
                }, Constants.RELOAD_DURATION);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    private Handler mUiHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case 0:
                    initialUI();
                    break;
                case 1:
//                    if(mAdBannerView != null && mBannerParent != null) {
//                        mAdBannerView.loadAd();
//                        mBannerParent.addView(mAdBannerView);
//                    }
            }
        }
    };

    public void setRepickLike(boolean isRepickLike) {
        this.mIsRepickLike = isRepickLike;
    }

    public FireStoreRequest getFirestoreRequest(){
        if(mFireStoreRequest == null){
            mFirestore = FirebaseFirestore.getInstance();
            mFireStoreRequest = new FireStoreRequest(mFirestore);
        }
        return mFireStoreRequest;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void gotoFragment(FragmentPage page){
        mLastFragment = mCurrentFragment;
        if (mCurrentFragment != null &&
                ((mCurrentFragment.getClass().getSimpleName().equals(ClassifyFragment.class.getSimpleName()) && page == FragmentPage.PAGE_POST)
                        || (mCurrentFragment.getClass().getSimpleName().equals(PostsFragment.class.getSimpleName()) && page == FragmentPage.PAGE_CLASSIFY))) {
            popupBackFragment();
        }

        switch (page){
            case PAGE_HOME:
                mCurrentFragment = HomeFragment.getInstance();
                mTitle = getString(R.string.app_name);
                mRightBtn.setVisibility(View.GONE);
                break;
            case PAGE_CLASSIFY:
                mCurrentFragment = new ClassifyFragment();
                mRightBtn.setVisibility(View.GONE);
                break;
            case PAGE_POST:
                mCurrentFragment = new PostsFragment();
                mRightBtn.setVisibility(View.VISIBLE);
                mRightBtn.setImageResource(R.drawable.icon_settings);
                break;
            case PAGE_SEARCH_TEACH:
                mCurrentFragment = new SearchTeachFragment();
                mRightBtn.setVisibility(View.GONE);
        }
        changeContent();
        mTitleView.setText(mTitle);
    }

    public void setClassMap(List<ClassifyData> classList){
        for (ClassifyData data:classList){
            mClassMap.put(data.get_id(), data);
        }
    }

    public HashMap<String, ClassifyData> getClassMap(){
        return mClassMap;
    }

    public List<LikeData> getLikeList() {
        return mLikeList;
    }

    public void setLikeList(List<LikeData> id) {
        this.mLikeList = id;
    }

    public void setCurrentClassify(ClassifyData data){
        mCurrentClassify = data;
        mTitle = data.getTitle();
    }

    public ClassifyData getCurrentClassify(){
        return mCurrentClassify;
    }

    @Override
    public void onResume(){
        super.onResume();
        if((mCurrentFragment == null || mCurrentFragment.getView() == null) && System.currentTimeMillis() - mLastCreateTime > 1000)
            initialUI();
    }

    @OnClick({R.id.toolbar_left, R.id.toolbar_right})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.toolbar_left:
                if(mIsRepickLike && getCurrentFragment().getClass().getSimpleName().equals(ClassifyFragment.class.getSimpleName()))
                    gotoFragment(FragmentPage.PAGE_POST);
                else
                    onBackPressed();
                break;
            case R.id.toolbar_right:
                gotoFragment(FragmentPage.PAGE_CLASSIFY);
                if(getCurrentFragment().getClass().getSimpleName().equals(PostsFragment.class.getSimpleName())){
                    mIsRepickLike = true;
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count > 0) {
            popupBackFragment();
        }
        else {
            PopupManager.showLeaveAppConfirm(this, getWindow().getDecorView(), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    private Fragment getCurrentFragment(){
        if(mFragmentManager.getBackStackEntryCount() > 0) {
            String fragmentTag = mFragmentManager.getBackStackEntryAt(mFragmentManager.getBackStackEntryCount() - 1).getName();
            Fragment currentFragment = mFragmentManager.findFragmentByTag(fragmentTag);
            return currentFragment;
        }
        return HomeFragment.getInstance();
    }

    @Override
    protected void onDestroy() {
        if (mAdBannerView != null) {
            mAdBannerView.destroy();
        }
        super.onDestroy();
    }
}
