package com.mgcoco.wallclassify.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.adapter.LikesAdapter;
import com.mgcoco.wallclassify.entity.response.ClassifyData;
import com.mgcoco.wallclassify.entity.response.LikeData;
import com.mgcoco.wallclassify.entity.response.PageData;
import com.mgcoco.wallclassify.manager.PopupManager;
import com.mgcoco.wallclassify.network.ApiCallbackListener;
import com.mgcoco.wallclassify.network.GraphApiRequest;
import com.mgcoco.wallclassify.view.activity.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by softicecan on 2018/3/4.
 */

public class ClassifyFragment extends BaseFragment {

    @BindView(R.id.classify_list)
    RecyclerView mList;
    @BindView(R.id.classify_next)
    View mNext;
    @BindView(R.id.classify_search_url)
    EditText mSearchEdit;


    private List<ClassifyData> mLocalClassifyList;
    private List<LikeData> mPickList = new ArrayList<>();
    private LikesAdapter mAdapter;
//    private static BaseFragment sInstance;
//
//    public static BaseFragment getInstance() {
//        if (sInstance == null)
//            sInstance = new ClassifyFragment();
//        return sInstance;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_classify, container, false);
        ButterKnife.bind(this, mView);
        initialUI();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshUI();
    }

    private void initialUI(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(layoutManager);
        mAdapter = new LikesAdapter(mContext);
        mList.setAdapter(mAdapter);

        if(AccessToken.getCurrentAccessToken() != null){
            mLocalClassifyList = getLocalClassifyList();
            mPickList.clear();

            GraphApiRequest.getInstance().getLikes(mContext, true, new ApiCallbackListener<List<LikeData>>() {
                @Override
                public void onSuccess(List<LikeData> data) {

                    if(((MainActivity)mContext).getClassMap() == null || ((MainActivity)mContext).getCurrentClassify() == null
                        || !((MainActivity)mContext).getClassMap().containsKey(((MainActivity)mContext).getCurrentClassify().get_id())) {
                        ((MainActivity)mContext).gotoFragment(MainActivity.FragmentPage.PAGE_HOME);
                        return;
                    }
                    final ClassifyData classifyData = ((MainActivity)mContext).getClassMap().get(((MainActivity)mContext).getCurrentClassify().get_id());


                    //預設與個人追蹤不重複才列入清單
                    HashMap<String, LikeData> defaultPageHM = getLikeDataHash(classifyData.getDefaultPage());
                    mPickList.addAll(classifyData.getDefaultPage());

                    for(LikeData ld:data) {
                        if(!defaultPageHM.containsKey(ld.getId()))
                            mPickList.add(ld);
                    }

                    //本地紀錄的喜好
                    HashMap<String, LikeData> localLikeMap = new HashMap<>();
                    if(mLocalClassifyList != null){
                        for(ClassifyData d:mLocalClassifyList){
                            if(d.get_id().equals(classifyData.get_id())){
                                localLikeMap = getLikeDataHash(d.getPickPage());
                                break;
                            }
                        }
                    }

                    //設置勾選
                    for(LikeData lk:mPickList){
                        if(localLikeMap.containsKey(lk.getId())) {
                            lk.setChecked(true);
                            localLikeMap.get(lk.getId()).setChecked(true);
                        }
                        else if(localLikeMap.containsKey(lk.getPageId())) {
                            lk.setChecked(true);
                            localLikeMap.get(lk.getPageId()).setChecked(true);
                        }
                    }
                    for (Map.Entry<String, LikeData> entry : localLikeMap.entrySet()) {
                        LikeData lk = entry.getValue();
                        if (!lk.isChecked()){
                            mPickList.add(0, lk);
                        }
                    }

                    mAdapter.setData(mPickList);
                    refreshConfirm();

                    mAdapter.setOnItemClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshConfirm();
                        }
                    });

                    mNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            List<LikeData> l = new ArrayList<>();
                            for(int i = 0; i < mPickList.size(); i++){
                                if(mPickList.get(i).isChecked()) {
                                    l.add(mPickList.get(i));
                                }
                            }
                            if(!mNext.isSelected()) {
                                boolean isSave = false;
                                if (mLocalClassifyList != null) {
                                    for (ClassifyData cd : mLocalClassifyList) {
                                        if (cd.get_id().equals(classifyData.get_id())) {
                                            isSave = true;
                                            cd.setPickPage(l);
                                            saveLocalClassifyList(mLocalClassifyList);
                                            break;
                                        }
                                    }
                                } else {
                                    mLocalClassifyList = new ArrayList<>();
                                }
                                if (!isSave) {
                                    classifyData.setPickPage(l);
                                    mLocalClassifyList.add(classifyData);
                                    saveLocalClassifyList(mLocalClassifyList);
                                }

                                ((MainActivity) mContext).setLikeList(l);
                                ((MainActivity) mContext).gotoFragment(MainActivity.FragmentPage.PAGE_POST);
                            }
                            else{
                                Toast.makeText(mContext, "請至少選擇一個項目", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                @Override
                public void onFailure(int status, String message) {

                }
            });
        }
    }

    private void refreshConfirm(){
        if(mAdapter != null) {
            if (mAdapter.getCheckCount() > 0)
                mNext.setSelected(false);
            else
                mNext.setSelected(true);
        }
    }

    @OnClick({R.id.classify_search_action, R.id.classify_search_teach})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.classify_search_action:
                if(mSearchEdit.getText() != null && mSearchEdit.getText().toString().startsWith("http")) {
                    GraphApiRequest.getInstance().findFacebookId(mContext, mSearchEdit.getText().toString(), true, new ApiCallbackListener<PageData>() {
                        @Override
                        public void onSuccess(final PageData data) {
                            for(LikeData lkd:mPickList){
                                if(data.getId().equals(lkd.getId()) || data.getId().equals(lkd.getPageId())){
                                    lkd.setChecked(true);
                                    mAdapter.notifyDataSetChanged();
                                    return;
                                }
                            }
                            PopupManager.showProfileInfo(mContext, mView, data, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    LikeData lkd = new LikeData();
                                    lkd.setId(data.getId());
                                    lkd.setName(data.getName());
                                    lkd.setChecked(true);
                                    mPickList.add(0, lkd);
                                    mAdapter.setData(mPickList);
                                    refreshConfirm();
                                }
                            });
                        }

                        @Override
                        public void onFailure(int status, String message) {

                        }
                    });
                }
                break;
            case R.id.classify_search_teach:
                ((MainActivity) mContext).gotoFragment(MainActivity.FragmentPage.PAGE_SEARCH_TEACH);
                break;
        }
    }

    @Override
    public void refreshUI() {

    }
}