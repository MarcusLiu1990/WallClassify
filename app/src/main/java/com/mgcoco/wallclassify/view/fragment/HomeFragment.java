package com.mgcoco.wallclassify.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.adapter.ClassifyAdapter;
import com.mgcoco.wallclassify.entity.response.ClassifyData;
import com.mgcoco.wallclassify.network.ApiCallbackListener;
import com.mgcoco.wallclassify.util.CommonUtil;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.PrefUtil;
import com.mgcoco.wallclassify.view.activity.MainActivity;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MarcusLiu on 2017/12/11.
 */

public class HomeFragment extends BaseFragment{

    @BindView(R.id.home_classify_list)
    RecyclerView mList;

    private CallbackManager mFbLoginCallbackManager;
    private ClassifyAdapter mAdapter;

    private static BaseFragment sInstance;

    public static BaseFragment getInstance(){
        if(sInstance == null)
            sInstance = new HomeFragment();
        return sInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, mView);
        initialUI();

        return mView;
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshUI();
    }

    private void initialUI(){
        initialFacebookLoginManager();
        mList.setHasFixedSize(true);
        mList.setLayoutManager(new GridLayoutManager(mContext, 2));

        mAdapter = new ClassifyAdapter(mContext);
        mList.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)mContext).setCurrentClassify((ClassifyData)v.getTag());
                PrefUtil.setStringPreference(mContext, Constants.Pref.LAST_STAY_CLASSIFY, new Gson().toJson(v.getTag()));
                if(AccessToken.getCurrentAccessToken() != null){
                    List<ClassifyData> localClassifyList = getLocalClassifyList();
                    Log.e("", "ClassifyData " + getLocalClassify() + ", " + ((ClassifyData)v.getTag()).get_id());
                    if(localClassifyList != null){
                        for(ClassifyData data:localClassifyList){
                            if(data.get_id().equals(((ClassifyData)v.getTag()).get_id())){
                                ((MainActivity)mContext).setLikeList(data.getDefaultPage());
                                ((MainActivity)mContext).gotoFragment(MainActivity.FragmentPage.PAGE_POST);
                                return;
                            }
                        }
                        ((MainActivity)mContext).gotoFragment(MainActivity.FragmentPage.PAGE_CLASSIFY);
                    }
                    else{
                        ((MainActivity)mContext).gotoFragment(MainActivity.FragmentPage.PAGE_CLASSIFY);
                    }
                }
                else{
                    LoginManager.getInstance().logInWithReadPermissions(HomeFragment.this, Arrays.asList("public_profile","email", "user_likes"));
                }
            }
        });

        if(PrefUtil.getStringPreference(mContext, Constants.Pref.CLASSIFY_CATCH) == null ||
                System.currentTimeMillis() - PrefUtil.getLongPreference(mContext, Constants.Pref.LAST_UPDATE_CLASSIFY_CATCH, 0) > 1000 * 60 * 60 * 24){
            ((MainActivity)mContext).getFirestoreRequest().getAllClassification(mContext, new ApiCallbackListener<List<ClassifyData>>() {
                @Override
                public void onSuccess(List<ClassifyData> data) {
                    PrefUtil.setStringPreference(mContext, Constants.Pref.CLASSIFY_CATCH, new Gson().toJson(data));
                    PrefUtil.setLongPreference(mContext, Constants.Pref.LAST_UPDATE_CLASSIFY_CATCH, System.currentTimeMillis());
                    ((MainActivity)mContext).setClassMap(data);
                    mAdapter.setData(data);
                }

                @Override
                public void onFailure(int status, String message) {

                }
            });

        }
        else{
            Gson gson = new Gson();
            List<ClassifyData> data = gson.fromJson(PrefUtil.getStringPreference(mContext, Constants.Pref.CLASSIFY_CATCH), new TypeToken<List<ClassifyData>>(){}.getType());
            ((MainActivity)mContext).setClassMap(data);
            mAdapter.setData(data);
        }
    }

    @Override
    public void refreshUI() {
        if(mContext == null)
            return;
    }


    private void initialFacebookLoginManager(){
        mFbLoginCallbackManager = CommonUtil.getFbLoginCallbackManager(new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject json, GraphResponse response) {
                if (response.getError() == null){
                    ((MainActivity)mContext).gotoFragment(MainActivity.FragmentPage.PAGE_CLASSIFY);
                }
                else{
                    Toast.makeText(mContext, "Login error:" + response.getError().getErrorMessage(), Toast.LENGTH_SHORT).show();
//                    Log.e("", "Login error:" + response.getError().getErrorMessage());
                }
            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mFbLoginCallbackManager == null)
            initialFacebookLoginManager();
        mFbLoginCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

}