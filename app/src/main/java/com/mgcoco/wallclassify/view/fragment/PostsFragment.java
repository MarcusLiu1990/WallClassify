package com.mgcoco.wallclassify.view.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.facebook.AccessToken;
import com.facebook.GraphRequestAsyncTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.adapter.PostsAdapter;
import com.mgcoco.wallclassify.entity.response.LikeData;
import com.mgcoco.wallclassify.entity.response.PostData;
import com.mgcoco.wallclassify.network.ApiCallbackListener;
import com.mgcoco.wallclassify.network.GraphApiRequest;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.PrefUtil;
import com.mgcoco.wallclassify.view.activity.MainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by softicecan on 2018/3/6.
 */

public class PostsFragment extends BaseFragment {

    @BindView(R.id.posts_list)
    RecyclerView mList;

    private PostsAdapter mAdapter;
    private int mTotalRequest;
    private List<PostData> mData = new ArrayList<>();
    private List<GraphRequestAsyncTask> mTaskArray = new ArrayList<>();
    private SimpleDateFormat mSdf;
    private String mCurrentId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_posts, container, false);
        ButterKnife.bind(this, mView);
        initialUI();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshUI();
    }

    private void initialUI(){
        if(((MainActivity)mContext).getCurrentClassify() == null) {
            ((MainActivity)mContext).gotoFragment(MainActivity.FragmentPage.PAGE_HOME);
            return;
        }
        ((MainActivity)mContext).setRepickLike(false);
        mSdf = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss", Locale.ENGLISH);
        mSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        mData.clear();
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(layoutManager);
        mAdapter = new PostsAdapter(mContext);

        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostData postData = (PostData) v.getTag();
                Intent i = new Intent(Intent.ACTION_VIEW);
                String[] ids = postData.getId().split("_");
                if(ids != null && ids.length == 2) {
                    i.setData(Uri.parse("https://www.facebook.com/" + ids[0] + "/posts/" + ids[1]));
                    startActivity(i);
                }
            }
        });

        mAdapter.setOnPageClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenFacebookPage(((PostData) v.getTag()).getPageId());
            }
        });
        mList.setAdapter(mAdapter);

        boolean isShowLoading = true;
        mCurrentId = ((MainActivity)mContext).getCurrentClassify().get_id();
        String catchData = PrefUtil.getStringPreference(mContext, mCurrentId);
        if(catchData != null){
            isShowLoading = false;
            Gson gson = new Gson();
            mAdapter.setData((List<PostData>)gson.fromJson(catchData, new TypeToken<List<PostData>>(){}.getType()));
            mList.scrollToPosition(PrefUtil.getIntegerPreference(mContext, mCurrentId + Constants.Pref.LAST_STAY_POSITION, 0));
            System.out.println(new Gson().toJson(catchData));
        }

        List<LikeData> likeData = ((MainActivity)mContext).getLikeList();
        if(AccessToken.getCurrentAccessToken() != null){
            mTotalRequest = likeData.size();
            for(int i = 0; i < mTotalRequest; i++) {
                String requestId = likeData.get(i).getId();
                if(likeData.get(i).getPageId() != null)
                    requestId = likeData.get(i).getPageId();
                GraphRequestAsyncTask task = GraphApiRequest.getInstance().getPosts(mContext, requestId, isShowLoading, new ApiCallbackListener<List<PostData>>() {
                    @Override
                    public void onSuccess(List<PostData> data) {
                        mData.addAll(data);
                        mTotalRequest --;
                        if(mTotalRequest == 0) {
                            Collections.sort(mData, new TimeComparator());
                            Collections.reverse(mData);
                            List<PostData> list = new ArrayList<>();
                            for(int i = 0; i < mData.size(); i++){
                                if(i % 4 == 3) {
                                    PostData pData = new PostData();
                                    pData.setNativeAds(true);
                                    list.add(pData);
                                }
                                list.add(mData.get(i));
                            }

                            if(mAdapter.getData() != null && mAdapter.getData().size() > 5 && list.size() > 5
                                    && mAdapter.getData().size() == list.size()){
                                boolean needUpdate = false;
                                for(int i = 0; i < 5; i++){
                                    if(mAdapter.getData().get(i).getId() != null && !mAdapter.getData().get(i).getId().equals(list.get(i).getId())) {
                                        needUpdate = true;
                                        break;
                                    }
                                }
                                if(!needUpdate)
                                    return;
                            }

                            mAdapter.setData(list);
                            mTaskArray.clear();
                            PrefUtil.setStringPreference(mContext, mCurrentId, new Gson().toJson(list));
                            mList.scrollToPosition(0);
                        }
                    }

                    @Override
                    public void onFailure(int status, String message) {
                        mTotalRequest --;
                    }
                });
                mTaskArray.add(task);
            }
        }

        mList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
                    LinearLayoutManager layoutManager = ((LinearLayoutManager)recyclerView.getLayoutManager());
                    PrefUtil.setIntegerPreference(mContext, mCurrentId + Constants.Pref.LAST_STAY_POSITION, layoutManager.findFirstVisibleItemPosition());
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }


    public class TimeComparator implements Comparator<PostData> {
        @Override
        public int compare(PostData o1, PostData o2) {
            Date date1 = null, date2 = null;
            try {
                date1 = mSdf.parse(o1.getCreated_time());
                date2= mSdf.parse(o2.getCreated_time());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date1.compareTo(date2);
        }
    }

    @Override
    public void refreshUI() {

    }

    public void OpenFacebookPage(String facebookPageID){
        String facebookUrl = "https://www.facebook.com/" + facebookPageID;
        String facebookUrlScheme = "fb://page/" + facebookPageID;

        try {
            int versionCode = mContext.getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrlScheme)));
        }
        catch (PackageManager.NameNotFoundException e) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));

        }

    }

    @Override
    public void onDestroyView(){
        if(mTaskArray != null && mTaskArray.size() > 0){
            for(int i = 0; i < mTaskArray.size(); i++){
                if(mTaskArray.get(i).getStatus().equals(AsyncTask.Status.RUNNING)){
                    mTaskArray.get(i).cancel(true);
                }
            }
        }
        super.onDestroyView();
    }
}