package com.mgcoco.wallclassify.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.entity.response.ClassifyData;
import com.mgcoco.wallclassify.util.DeviceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/4.
 */

public class ClassifyAdapter extends RecyclerView.Adapter<ClassifyAdapter.ViewHolder> {

    private Context mContext;
    private View.OnClickListener mListener;
    private List<ClassifyData> mData = new ArrayList<>();
    private int mSize = 0;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mParent;
        public TextView title;
        public SimpleDraweeView img;
        public ViewHolder(View v) {
            super(v);
            mParent = v;
            title = v.findViewById(R.id.item_classify_name);
            img = v.findViewById(R.id.item_classify_img);

        }
    }//"1000091930154368_1019440684886159"

    public ClassifyAdapter(Context context) {
        mContext = context;
        mSize = DeviceUtil.getDisplay(context)[0] / 2;
    }

    public void setData(List<ClassifyData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(View.OnClickListener listener){
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_classify, parent, false);
        ViewHolder vh = new ViewHolder(v);
        vh.mParent.getLayoutParams().width = mSize;
        vh.mParent.getLayoutParams().height = mSize;
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.title.setText(mData.get(position).getTitle());
        holder.mParent.setTag(mData.get(position));
        holder.img.setImageURI(mData.get(position).getImage());

        if(position % 2 == 0)
            holder.title.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        else
            holder.title.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        if(mListener != null)
            holder.mParent.setOnClickListener(mListener);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}