package com.mgcoco.wallclassify.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.entity.response.LikeData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/2/4.
 */

public class LikesAdapter extends RecyclerView.Adapter<LikesAdapter.ViewHolder> {
    private Context mContext;
    private View.OnClickListener mListener;
    private List<LikeData> mData = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mParent;
        public TextView title, time;
        public SimpleDraweeView img;
        public ImageView check;
        public ViewHolder(View v) {
            super(v);
            mParent = v;
            title = v.findViewById(R.id.item_like_title);
//            time = v.findViewById(R.id.item_like_msg);
            img = v.findViewById(R.id.item_like_icon);
            check = v.findViewById(R.id.item_like_check);

        }
    }

    public LikesAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<LikeData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public int getCheckCount(){
        int count = 0;
        for (LikeData data:mData)
            if(data.isChecked())
                count++;
        return count;
    }

    public void setOnItemClickListener(View.OnClickListener listener){
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_like, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String name = mData.get(position).getName();
        holder.title.setText(name);
//        holder.time.setText(mData.get(position).getCreated_time());
        holder.mParent.setTag(mData.get(position));

        holder.check.setSelected(mData.get(position).isChecked());
        holder.check.setTag(position);
        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                mData.get((Integer) v.getTag()).setChecked(v.isSelected());
            }
        });

        if(mData.get(position).getPageId() != null)
            holder.img.setImageURI(mContext.getString(R.string.fb_profile_pic, mData.get(position).getPageId()));
        else
            holder.img.setImageURI(mContext.getString(R.string.fb_profile_pic, mData.get(position).getId()));

        holder.mParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.check.performClick();
                if(mListener != null)
                    mListener.onClick(v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}