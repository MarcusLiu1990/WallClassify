package com.mgcoco.wallclassify.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mgcoco.wallclassify.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/18.
 */

public class SearchTeachPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<String> mData = new ArrayList<>();
    private View.OnClickListener mListener;

    public SearchTeachPagerAdapter(Context context, List<String> data) {
        mContext = context;
        mData = data;
    }

    public void setOnClickListener(View.OnClickListener listener){
        mListener = listener;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_search_teach, collection, false);
        collection.addView(layout);
        SimpleDraweeView draweeView = layout.findViewById(R.id.item_search_teach_img);
        draweeView.setImageURI(mData.get(position));

        View ok = layout.findViewById(R.id.item_search_teach_ok);
        if(position == mData.size() - 1)
            ok.setVisibility(View.VISIBLE);
        else
            ok.setVisibility(View.GONE);
        ok.setOnClickListener(mListener);

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

}