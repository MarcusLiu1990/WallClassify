package com.mgcoco.wallclassify.adapter;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.entity.response.PostData;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.DeviceUtil;
import com.mgcoco.wallclassify.view.activity.MainActivity;
import com.mgcoco.wallclassify.widget.AdFacebook;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by softicecan on 2018/3/6.
 */

public class PostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private View.OnClickListener mListener, mOpenPageListener;
    private List<PostData> mData = new ArrayList<>();
    private HashMap<Integer, NativeAd> mAds = new HashMap<>();
    private int mMaxWidth;

    public final static int ITEM_TYPE_POST = 0;
    public final static int ITEM_TYPE_AD = 1;

    private AdFacebook mAdFacebook;
    private SimpleDateFormat mSdf, mSdfNow;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mParent, play, link;
        public TextView name, time, message, more;
        public SimpleDraweeView head, picture;
        public ViewHolder(View v) {
            super(v);
            mParent = v;
            name = v.findViewById(R.id.item_post_name);
            time = v.findViewById(R.id.item_post_time);
            message = v.findViewById(R.id.item_post_message);
            head = v.findViewById(R.id.item_post_head);
            picture = v.findViewById(R.id.item_post_pic);
            play = v.findViewById(R.id.item_post_play);
            more = v.findViewById(R.id.item_post_more);
            link = v.findViewById(R.id.item_post_link);
        }
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        public View mParent;
        public TextView name, time, message;
        public ImageView icon;
        public MediaView media;
        private RelativeLayout adChoice;

        public AdViewHolder(View v) {
            super(v);
            mParent = v;
            name = v.findViewById(R.id.item_ad_fb_name);
            time = v.findViewById(R.id.item_ad_fb_action);
            message = v.findViewById(R.id.item_ad_fb_desacription);
            icon = v.findViewById(R.id.item_ad_fb_icon);
            media = v.findViewById(R.id.item_ad_media);
            adChoice = v.findViewById(R.id.item_ad_choice_container);
        }
    }

    public PostsAdapter(Context context) {
        mContext = context;
        mMaxWidth = (int)(DeviceUtil.getDisplay(mContext)[0] * 0.9f);
        mAdFacebook = AdFacebook.getAdInstance((MainActivity)context);

        mSdf = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss", Locale.ENGLISH);
        mSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        mSdfNow = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        mSdfNow.setTimeZone(TimeZone.getDefault());
    }

    public void setData(List<PostData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public List<PostData> getData(){
        return mData;
    }

    public void setOnItemClickListener(View.OnClickListener listener){
        mListener = listener;
    }

    public void setOnPageClickListener(View.OnClickListener listener){
        mOpenPageListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if(viewType == ITEM_TYPE_POST) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.item_post, parent, false);
            vh = new ViewHolder(v);
            ((View)((ViewHolder)vh).picture.getParent()).getLayoutParams().width = mMaxWidth;
        }
        else{
            View v = LayoutInflater.from(mContext).inflate(R.layout.item_ad_fb, parent, false);
            vh = new AdViewHolder(v);
            ((AdViewHolder)vh).media.getLayoutParams().width = mMaxWidth;
            ((AdViewHolder)vh).media.getLayoutParams().height = (int)(mMaxWidth * 0.7f);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof AdViewHolder) {
            AdViewHolder holder = (AdViewHolder) viewHolder;
            if(mAds.containsKey(position) && mAds.get(position) != null && mAds.get(position).isAdLoaded()){
                mAdFacebook.setAdViewContent(mAds.get(position), holder.mParent);
            }
            else{
                mAds.put(position, mAdFacebook.loadNativeAd(holder.mParent, "1000091930154368_1007453226084905", null));
                holder.media.getLayoutParams().height = (int)(mMaxWidth * 0.7f);
            }
        }
        else{
            PostData postData = mData.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.name.setText(postData.getName());

            holder.message.setAutoLinkMask(Linkify.EMAIL_ADDRESSES|Linkify.WEB_URLS);
            holder.message.setMovementMethod(LinkMovementMethod.getInstance());

            if (postData.getType().equals(Constants.PostType.LINK) || (postData.getMessage() == null && postData.getLink() != null)
                    || (postData.getMessage().length() == 0 && postData.getLink().length() > 0)) {

                if(postData.getMessage() != null && postData.getMessage().length() > 0)
                    holder.message.setText(postData.getMessage() + "\n" +postData.getLink());
                else
                    holder.message.setText(postData.getLink());
                holder.link.setVisibility(View.VISIBLE);
            }
            else {
                holder.message.setText(postData.getMessage());
                holder.link.setVisibility(View.GONE);
            }
            try {
                Date date = mSdf.parse(postData.getCreated_time());
                holder.time.setText(mSdfNow.format(new Date(date.getTime())));
            }
            catch (ParseException e){}
            holder.picture.setTag(postData);
            holder.name.setTag(postData);
            holder.name.setOnClickListener(mOpenPageListener);


            ViewTreeObserver vto = holder.message.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    holder.message.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    boolean isEllips = false;
                    if(holder.message.getTag() == null) {
                        Layout layout = holder.message.getLayout();
                        if (layout != null) {
                            int lines = layout.getLineCount();
                            if (lines > 0) {
                                if (layout.getEllipsisCount(lines - 1) > 0)
                                    isEllips = true;
                            }
                        }
                    }

                    if(isEllips) {
                        holder.more.setVisibility(View.VISIBLE);
                        holder.more.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                holder.more.setVisibility(View.GONE);
                                holder.message.setEllipsize(null);
                                holder.message.setMaxLines(100);
                                holder.message.setTag(true);
                            }
                        });
                    }
                    else
                        holder.more.setVisibility(View.GONE);
                }
            });


            if(holder.message.getTag() != null && (Boolean) holder.message.getTag()){
                holder.message.setEllipsize(null);
                holder.message.setMaxLines(50);
                holder.message.setTag(true);
            }
            else{
                holder.message.setEllipsize(TextUtils.TruncateAt.END);
                holder.message.setMaxLines(5);
                holder.message.setTag(null);
            }

            holder.head.setImageURI(mContext.getString(R.string.fb_profile_pic, postData.getPageId()));
            if(postData.getType() != null && postData.getType().equals(Constants.PostType.VIDEO))
                holder.play.setVisibility(View.VISIBLE);
            else
                holder.play.setVisibility(View.GONE);

            if(postData.getType().equals(Constants.PostType.STATUS) || postData.getFull_picture() == null)
                ((View)holder.picture.getParent()).setVisibility(View.GONE);
            else{
                ((View)holder.picture.getParent()).setVisibility(View.VISIBLE);
                ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable anim) {
                        if (imageInfo == null) {
                            holder.picture.setVisibility(View.GONE);
                            return;
                        }

                        LinearLayout.LayoutParams params = ((LinearLayout.LayoutParams)((View)holder.picture.getParent()).getLayoutParams());
                        params.height = (int)(mMaxWidth * ((float)imageInfo.getHeight() / (float)imageInfo.getWidth()));

                        ((View)holder.picture.getParent()).setLayoutParams(params);
                    }

                    @Override
                    public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                    }

                    @Override
                    public void onFailure(String id, Throwable throwable) {
                    }
                };

                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setControllerListener(controllerListener)
                        .setUri(postData.getFull_picture())
                        .build();
                holder.picture.setController(controller);
            }

            if(mListener != null)
                holder.picture.setOnClickListener(mListener);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).isNativeAds() ? ITEM_TYPE_AD : ITEM_TYPE_POST;
    }

}