package com.mgcoco.wallclassify.entity.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/4.
 */

public class ClassifyData {
    String _id;
    String title;
    String description;
    String image;
    List<LikeData> defaultPage = new ArrayList<>();
    List<LikeData> pickPage = new ArrayList<>();
    int order;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<LikeData> getDefaultPage() {
        return defaultPage;
    }

    public void setDefaultPage(List<LikeData> defaultPage) {
        this.defaultPage = defaultPage;
    }

    public List<LikeData> getPickPage() {
        return pickPage;
    }

    public void setPickPage(List<LikeData> pickPage) {
        this.pickPage = pickPage;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
