package com.mgcoco.wallclassify.entity.response;

/**
 * Created by MarcusLiu on 2017/12/26.
 */

public class BasicInfoData {

    String androidVersion;
    String currentAmount;
    String currentReward;
    String rankReward;
    int totalLottoryAmount;
    String recommandedReward;
    ServerMessage serverMessage;

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(String currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getCurrentReward() {
        return currentReward;
    }

    public void setCurrentReward(String currentReward) {
        this.currentReward = currentReward;
    }

    public int getTotalLottoryAmount() {
        return totalLottoryAmount;
    }

    public void setTotalLottoryAmount(int totalLottoryAmount) {
        this.totalLottoryAmount = totalLottoryAmount;
    }

    public String getRankReward() {
        return rankReward;
    }

    public void setRankReward(String rankReward) {
        this.rankReward = rankReward;
    }

    public String getRecommandedReward() {
        return recommandedReward;
    }

    public void setRecommandedReward(String recommandedReward) {
        this.recommandedReward = recommandedReward;
    }

    public ServerMessage getServerMessage() {
        return serverMessage;
    }

    public void setServerMessage(ServerMessage serverMessage) {
        this.serverMessage = serverMessage;
    }
}
