package com.mgcoco.wallclassify.entity.response;

/**
 * Created by softicecan on 2018/3/4.
 */

public class PictureData {

    private Detail picture;
    private String id;


    public Detail getPicture() {
        return picture;
    }

    public void setPicture(Detail picture) {
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public class Detail{
        int height;
        int width;
        boolean is_silhouette;
        String url;

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public boolean isIs_silhouette() {
            return is_silhouette;
        }

        public void setIs_silhouette(boolean is_silhouette) {
            this.is_silhouette = is_silhouette;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
