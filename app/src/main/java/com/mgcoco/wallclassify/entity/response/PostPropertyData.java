package com.mgcoco.wallclassify.entity.response;

/**
 * Created by softicecan on 2018/3/11.
 */

public class PostPropertyData {
    String name;
    String text;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
// "name": "Length",
//         "text": "01:21"