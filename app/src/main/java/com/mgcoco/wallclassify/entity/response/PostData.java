package com.mgcoco.wallclassify.entity.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/4.
 */

public class PostData {

    String link = "";
    String message = "";
    String full_picture;
    String created_time;
    String id;
    String pageId;
    PlaceData place;
    String name;
    List<PostPropertyData> properties = new ArrayList<>();
    boolean isNativeAds = false;
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFull_picture() {
        return full_picture;
    }

    public void setFull_picture(String full_picture) {
        this.full_picture = full_picture;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PlaceData getPlace() {
        return place;
    }

    public void setPlace(PlaceData place) {
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public boolean isNativeAds() {
        return isNativeAds;
    }

    public void setNativeAds(boolean nativeAds) {
        isNativeAds = nativeAds;
    }

    public List<PostPropertyData> getProperties() {
        return properties;
    }

    public void setProperties(List<PostPropertyData> properties) {
        this.properties = properties;
    }
}
