package com.mgcoco.wallclassify.entity.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/4.
 */

public class PageData {
    List<PostData> posts = new ArrayList<>();
    String id;
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PostData> getPosts() {
        return posts;
    }

    public void setPosts(List<PostData> posts) {
        this.posts = posts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
