package com.mgcoco.wallclassify.entity.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/2/4.
 */

public class NoticeData {

    List<Notice> notice = new ArrayList<>();

    public List<Notice> getNotice() {
        return notice;
    }

    public void setNotice(List<Notice> notice) {
        this.notice = notice;
    }

    public class Notice{
        String title;
        String content;
        String createTime;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }

}
