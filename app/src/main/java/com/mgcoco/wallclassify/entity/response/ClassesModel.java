package com.mgcoco.wallclassify.entity.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/10.
 */

public class ClassesModel {
    List<ClassifyData> classes = new ArrayList<>();

    public List<ClassifyData> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassifyData> classes) {
        this.classes = classes;
    }
}
