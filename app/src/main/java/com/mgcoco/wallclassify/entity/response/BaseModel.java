package com.mgcoco.wallclassify.entity.response;

/**
 * Created by softicecan on 2017/12/25.
 */

public class BaseModel<T> {
    int status;
    String message;
    T data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
