package com.mgcoco.wallclassify.entity.response;

/**
 * Created by softicecan on 2018/3/4.
 */

public class PagingData {

    String next;
    Cursor cursors;

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public Cursor getCursors() {
        return cursors;
    }

    public void setCursors(Cursor cursors) {
        this.cursors = cursors;
    }

    public class Cursor{
        String before;
        String after;

        public String getBefore() {
            return before;
        }

        public void setBefore(String before) {
            this.before = before;
        }

        public String getAfter() {
            return after;
        }

        public void setAfter(String after) {
            this.after = after;
        }
    }

}
