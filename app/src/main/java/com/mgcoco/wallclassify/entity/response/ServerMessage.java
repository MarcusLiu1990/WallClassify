package com.mgcoco.wallclassify.entity.response;

/**
 * Created by softicecan on 2018/2/22.
 */

public class ServerMessage {

    boolean isShowServerMessage;
    boolean isAlwaysShow;
    String message;

    public boolean isShowServerMessage() {
        return isShowServerMessage;
    }

    public void setShowServerMessage(boolean showServerMessage) {
        isShowServerMessage = showServerMessage;
    }

    public boolean isAlwaysShow() {
        return isAlwaysShow;
    }

    public void setAlwaysShow(boolean alwaysShow) {
        isAlwaysShow = alwaysShow;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
