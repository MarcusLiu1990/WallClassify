package com.mgcoco.wallclassify.manager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.mgcoco.wallclassify.R;
import com.mgcoco.wallclassify.entity.response.PageData;
import com.mgcoco.wallclassify.view.activity.BaseActivity;
import com.mgcoco.wallclassify.widget.AdFacebook;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/11.
 */


public class PopupManager {

    public interface onPopupVisiblityChangeListener{
        void onChange(boolean isShow, PopupWindow popupWindow);
    }

    private static List<onPopupVisiblityChangeListener> sOnPopupVisiblityChangeListenerList = new ArrayList<>();

    private static AlertDialog sAlertDialog;

    private static ArrayList<PopupWindow> sPopupCache = new ArrayList<>();

    public static void putIntoPopupCache(PopupWindow popup){
        sPopupCache.add(popup);
    }

    public static void addPopupVisiblityChangeListener(onPopupVisiblityChangeListener listener){
        sOnPopupVisiblityChangeListenerList.add(listener);
    }

    public static void dismissAllDialog(){
        if(sPopupCache != null) {
            for (int i = 0; i < sPopupCache.size(); i++) {
                if (sPopupCache.get(i) != null)
                    sPopupCache.get(i).dismiss();
            }
            sPopupCache.clear();
        }
        if(sOnPopupVisiblityChangeListenerList != null && sOnPopupVisiblityChangeListenerList.size() > 0){
            for(onPopupVisiblityChangeListener listener:sOnPopupVisiblityChangeListenerList){
                if(listener != null)
                    listener.onChange(false, null);
            }
        }
    }

    private static void dismissDialog(PopupWindow popup){
        if(sPopupCache != null) {
            for (int i = 0; i < sPopupCache.size(); i++) {
                if (sPopupCache.get(i) == popup) {
                    sPopupCache.get(i).dismiss();
                    sPopupCache.remove(popup);
                }
            }
        }
        if(sOnPopupVisiblityChangeListenerList != null && sOnPopupVisiblityChangeListenerList.size() > 0){
            for(onPopupVisiblityChangeListener listener:sOnPopupVisiblityChangeListenerList){
                if(listener != null)
                    listener.onChange(false, null);
            }
        }
    }

    public static boolean isAnyPopupShow(){
        if(sPopupCache != null){
            for (int i = 0; i < sPopupCache.size(); i++) {
                if (sPopupCache.get(i) != null && sPopupCache.get(i).isShowing())
                    return true;
            }
        }
        return false;
    }

    public static void initialBasicPopupAttr(final Activity context, final View view){

        final PopupWindow popup = new PopupWindow(view, context.getResources().getDisplayMetrics().widthPixels,
                context.getResources().getDisplayMetrics().heightPixels, true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popup.setAnimationStyle(R.style.popupAnim);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.update();

        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.alpha = 0.1f;
        context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        context.getWindow().setAttributes(lp);
        popup.setTouchable(true);
        popup.setOutsideTouchable(false);
        popup.showAtLocation(context.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
        putIntoPopupCache(popup);
        if(sOnPopupVisiblityChangeListenerList != null && sOnPopupVisiblityChangeListenerList.size() > 0){
            for(onPopupVisiblityChangeListener listener:sOnPopupVisiblityChangeListenerList){
                Log.e("", "change listener:" + listener);
                if(listener != null)
                    listener.onChange(true, popup);
            }
        }

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = context.getWindow().getAttributes();
                lp.alpha = 1f;
                context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                context.getWindow().setAttributes(lp);
                dismissDialog(popup);
            }
        });
    }

    public interface onLevelSelectListener{
        void onLevelSeleted(int[] level);
    }


    public static class DialogParams{
        public String title;
        public String message;
        public String leftText;
        public String rightText;
        public View.OnClickListener leftListener;
        public View.OnClickListener rightListener;
        public boolean cancelable = false;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getLeftText() {
            return leftText;
        }

        public void setLeftText(String leftText) {
            this.leftText = leftText;
        }

        public String getRightText() {
            return rightText;
        }

        public void setRightText(String rightText) {
            this.rightText = rightText;
        }

        public View.OnClickListener getLeftListener() {
            return leftListener;
        }

        public void setLeftListener(View.OnClickListener leftListener) {
            this.leftListener = leftListener;
        }

        public View.OnClickListener getRightListener() {
            return rightListener;
        }

        public void setRightListener(View.OnClickListener rightListener) {
            this.rightListener = rightListener;
        }

        public boolean isCancelable() {
            return cancelable;
        }

        public void setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
        }
    }

    public static void initialBasicPopupAttr(final Context context, final View view, View parent, int width, int height, int anim){
        initialBasicPopupAttr(context, view, parent, width, height, anim, Gravity.CENTER, true);
    }

    public static void initialBasicPopupAttr(final Context context, final View view, View parent, int width, int height, int anim, int gravity, boolean cancelable){

        final PopupWindow popup = new PopupWindow(view, width, height, true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setAnimationStyle(anim);
        popup.update();

        if(context instanceof Activity){
            WindowManager.LayoutParams lp = ((Activity)context).getWindow().getAttributes();
            lp.alpha = 0.4f;
            ((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            ((Activity)context).getWindow().setAttributes(lp);
            popup.setTouchable(true);
            popup.setOutsideTouchable(cancelable);

        }
        popup.showAtLocation(parent, gravity, 0, 0);
        putIntoPopupCache(popup);

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if(context instanceof Activity){
                    WindowManager.LayoutParams lp = ((Activity)context).getWindow().getAttributes();
                    lp.alpha = 1f;
                    ((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    ((Activity)context).getWindow().setAttributes(lp);
                }
                dismissDialog(popup);
            }
        });

    }

    public static void showTwoBtnDialog(final Context context, View parent, final DialogParams params){
        PopupManager.dismissAllDialog();

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_default_two_btn, null);
        TextView title = (TextView) view.findViewById(R.id.default_two_btn_title);
        TextView message = (TextView) view.findViewById(R.id.default_two_btn_message);
        TextView leftBtn = (TextView) view.findViewById(R.id.default_two_btn_left);
        TextView rightBtn = (TextView) view.findViewById(R.id.default_two_btn_right);
        View divider = view.findViewById(R.id.default_two_btn_divider);

        if(params.getTitle() != null)
            title.setText(params.getTitle());
        else
            title.setVisibility(View.GONE);

        if(params.getMessage() != null)
            message.setText(params.getMessage());
        else
            message.setVisibility(View.GONE);

        if(params.getLeftListener() == null){
            leftBtn.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            rightBtn.setBackgroundResource(R.drawable.selector_btn_bottom_corner);
        }
        else{
            leftBtn.setText(params.getLeftText());
            leftBtn.setOnClickListener(params.getLeftListener());
        }

        if(params.getRightListener() == null){
            rightBtn.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            leftBtn.setBackgroundResource(R.drawable.selector_btn_bottom_corner);
        }
        else{
            rightBtn.setText(params.getRightText());
            rightBtn.setOnClickListener(params.getRightListener());
        }
        initialBasicPopupAttr(context, view, parent, (int)(context.getResources().getDisplayMetrics().widthPixels * 0.9), WindowManager.LayoutParams.WRAP_CONTENT, R.style.anim_popup_fade, Gravity.CENTER, true);
    }

    public static void showProfileInfo(final Context context, View parent, PageData data, final View.OnClickListener listener){
        PopupManager.dismissAllDialog();

        if(data.getName() == null){
            Toast.makeText(context, "找不到此頁面Facebook ID，請輸入其他網址", Toast.LENGTH_SHORT).show();
            return;
        }

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_profile_find, null);
        TextView name = view.findViewById(R.id.profile_find_name);
        SimpleDraweeView head = view.findViewById(R.id.profile_find_head);

        name.setText(data.getName());
        head.setImageURI(context.getString(R.string.fb_profile_pic, data.getId()));

        view.findViewById(R.id.profile_find_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllDialog();
                listener.onClick(v);
            }
        });

        view.findViewById(R.id.profile_find_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllDialog();
            }
        });
        initialBasicPopupAttr(context, view, parent, (int)(context.getResources().getDisplayMetrics().widthPixels * 0.9), WindowManager.LayoutParams.WRAP_CONTENT, R.style.anim_popup_fade, Gravity.CENTER, true);
    }

    private static View sLeaveAppConfirmView;

    public static void loadLeaveAppConfirmView(final Context context, final View.OnClickListener listener){
        sLeaveAppConfirmView = LayoutInflater.from(context).inflate(R.layout.dialog_leave, null);
        sLeaveAppConfirmView.findViewById(R.id.dialog_leave_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllDialog();
                loadLeaveAppConfirmView(context, listener);
            }
        });
        sLeaveAppConfirmView.findViewById(R.id.dialog_leave_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupManager.dismissAllDialog();
                listener.onClick(v);
            }
        });
        AdFacebook.getAdInstance((BaseActivity)context).loadNativeAd(sLeaveAppConfirmView, "1000091930154368_1019414211555473", new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {

            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    public static void showLeaveAppConfirm(Context context, View parent, final View.OnClickListener listener){
        PopupManager.dismissAllDialog();

        if(sLeaveAppConfirmView == null)
            loadLeaveAppConfirmView(context, listener);

        initialBasicPopupAttr(context, sLeaveAppConfirmView, parent, (int)(context.getResources().getDisplayMetrics().widthPixels * 0.9),
                WindowManager.LayoutParams.WRAP_CONTENT, R.style.anim_popup_fade, Gravity.CENTER, false);
    }

    public static void showRequestErrorMassage(final Context context, View parent, String message){
        DialogParams params = new DialogParams();
        params.setTitle("錯誤訊息");
        params.setMessage(message);
        params.setRightText(context.getString(R.string.general_confirm));
        params.setRightListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupManager.dismissAllDialog();
            }
        });
        PopupManager.showTwoBtnDialog(context, parent, params);
    }
}
