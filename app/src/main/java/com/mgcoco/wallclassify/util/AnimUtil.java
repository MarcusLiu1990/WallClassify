package com.mgcoco.wallclassify.util;

import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

/**
 * Created by MarcusLiu on 2017/12/7.
 */

public class AnimUtil {

    public static final int DURATION_FAST = 250;
    public static final int DURATION_NORMAL = 400;
    public static final int DURATION_SLOW = 600;

    public static void startAminIn(Techniques techniques, int duration, View view, Animator.AnimatorListener listener){
        view.setVisibility(View.VISIBLE);
        if(listener != null)
            YoYo.with(techniques).duration(duration).withListener(listener).playOn(view);
        else
            YoYo.with(techniques).duration(duration) .playOn(view);
    }

    public static void startAnimOut(Techniques techniques, int duration, final View view, final Animator.AnimatorListener listener){
        YoYo.with(techniques).duration(duration).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
                if(listener != null)
                    listener.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).playOn(view);
    }

}
