package com.mgcoco.wallclassify.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;

/**
 * Created by MarcusLiu on 2017/12/11.
 */

public class ImgUtil {

    public static Bitmap getCircleBitmap(int size){
        int width = size;
        int height = size;
        Bitmap bg = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bg);
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.argb(160, 88, 202, 244));
        canvas.drawCircle(width / 2, height / 2, width / 2, p);

        p.setColor(Color.argb(128, 0, 0, 0));
        canvas.drawCircle(width / 2, height / 2, (int)(width * 0.375f), p);
        return bg;
    }

    public static Bitmap getCard(int width){
        int height = (int)(width * 1.68f);
        Bitmap bg = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bg);
        Paint p = new Paint();
        RectF rectF = new RectF(0, 0, width, height);
        p.setAntiAlias(true);
        p.setColor(Color.rgb(13, 37, 76));
        canvas.drawRoundRect(rectF, width * 0.075f, width * 0.075f, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawCircle(width / 2, 0, width * 0.113f, p);

        float rectFFWidth = (float)width / 16f;
        float rectFFHeight = rectFFWidth * 0.78f;
        float start = -rectFFWidth / 2;

        float heightOffset = height * 0.6f;
        for(int i = 0; i < 10; i ++){
            canvas.drawRoundRect(new RectF(start, heightOffset, start + rectFFWidth, heightOffset + rectFFHeight), width, width, p);
            start += rectFFWidth * 2f;
        }
        return bg;
    }
}
