package com.mgcoco.wallclassify.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.List;

/**
 * Created by MarcusLiu on 2017/7/13.
 */

public class CommonUtil {

    private static final String TAG = CommonUtil.class.getSimpleName();

    public static void gotoGooglePlay(Context context){
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void openFacebookPage(Context context, String facebookPageID){
        String facebookUrl = "https://www.facebook.com/" + facebookPageID;
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
    }

    public static String getVersionCode(Context context){
        String appVersion = "";
        PackageManager manager = context.getPackageManager();
        try { PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            appVersion = info.versionName; //版本名
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appVersion;
    }
    public static String convertTimeFormat(String dateTime){
        if(dateTime != null && dateTime.length() > 8)
            return dateTime.substring(0, dateTime.length() - 8).replace("T", " ");
        return dateTime;
    }

    public static void gotoGoogleMap(Context context, String lat, String lon) {
        Uri gmmIntentUri = Uri.parse("https://www.google.com/maps?saddr=My+Location&daddr=" + lat + "," + lon);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(mapIntent);
        }
    }

    public static void measureView(View view){
        int width = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int height = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(width, height);
    }

    public static String ignoreUnusedDigit(float number){
        String value = number + "";
        if(value.endsWith(".0")){
            return value.replace(".0", "");
        }
        return value;
    }

    public static boolean isAppOnForeground(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public static CallbackManager getFbLoginCallbackManager(final GraphRequest.GraphJSONObjectCallback callback){
        CallbackManager callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            //登入成功
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Log.e("", "Login getFbLoginCallbackManager");
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), callback);
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,first_name,last_name");
                request.setGraphPath("me");
                request.setParameters(parameters);
                request.executeAsync();
            }

            //登入取消
            @Override
            public void onCancel() {
                Log.e("", "Login cancel");
            }

            //登入失敗
            @Override
            public void onError(FacebookException exception) {
                Log.e("", "Login " + exception.toString());
            }
        });
        return callbackManager;
    }

//    public static void setSimpleDraweeViewUri(SimpleDraweeView view, Object res) {
//        if(view != null && res != null) {
//            Uri uri;
//            if(res instanceof String) {
//                if(((String)res).startsWith("http")) {
//                    uri = Uri.parse((String)res);
//                } else {
//                    uri = (new Uri.Builder()).scheme("file").path((String)res).build();
//                }
//            } else {
//                uri = (new Uri.Builder()).scheme("res").path(String.valueOf(res)).build();
//            }
//
//            view.setImageURI(uri);
//        }
//    }

//    public static boolean isAppExist(Activity context, String packageName) {
//        PackageManager manager = context.getPackageManager();
//        Intent intent = (new Intent()).setPackage(packageName);
//        List infos = manager.queryIntentActivities(intent, 65536);
//        return infos != null && (infos == null || infos.size() >= 1);
//    }

    public static float convertDpToPx(float dp, Context context) {
        float px = dp * getDensity(context);
        return px;
    }

    public static float convertPxToDp(float px, Context context) {
        float dp = px / getDensity(context);
        return dp;
    }

    public static float getDensity(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }
}
