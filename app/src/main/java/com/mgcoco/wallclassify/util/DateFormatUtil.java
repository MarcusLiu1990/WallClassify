package com.mgcoco.wallclassify.util;

import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

/**
 * Created by softicecan on 2017/12/18.
 */

public class DateFormatUtil {

    public static String getTime(long milliSeconds){
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds))
        );
    }

    public static String getDateTime(long time){
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.TIME_FORMAT);
        return sdf.format(time);
    }
}
