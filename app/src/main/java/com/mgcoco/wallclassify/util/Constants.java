package com.mgcoco.wallclassify.util;

/**
 * Created by MarcusLiu on 2017/12/7.
 */

public class Constants {

    public static final String DOMAIN_URL =  "https://mgcoco-design.com/api/v1/";
//    public static final String DOMAIN_URL = "http://104.155.207.253/api/v1/";


    public static final String TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    public static final String AD_ID_FB_HOME_BANNER = "1000091930154368_1007452929418268";
    public static final int RETRY_DURATION = 5000;
    public static final int RELOAD_DURATION = 30 * 1000;


    public static class PostType{
        public static final String VIDEO = "video";
        public static final String PHOTO = "photo";
        public static final String LINK = "link";
        public static final String STATUS = "status";
        public static final String OFFER = "offer";
    }

    public static class Pref{
        public static final String FCM_TOKEN = "FCM_TOKEN";
        public static final String IS_AGREE_TERMS = "IS_AGREE_TERMS";


        public static final String LOCAL_CLASSIFY = "LOCAL_CLASSIFY";
        public static final String LAST_STAY_PAGE = "LAST_STAY_PAGE";
        public static final String LAST_STAY_CLASSIFY = "LAST_STAY_CLASSIFY";
        public static final String LAST_STAY_POSITION = "LAST_STAY_POSITION";

        public static final String CLASSIFY_CATCH = "CLASSIFY_CATCH";
        public static final String LAST_UPDATE_CLASSIFY_CATCH = "LAST_UPDATE_CLASSIFY_CATCH";
    }

}
