package com.mgcoco.wallclassify.widget;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.mgcoco.wallclassify.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by softicecan on 2018/3/10.
 */


public class AdFacebook {

    private static final String TAG = AdFacebook.class.getSimpleName();
    private Activity mActivity;
    private InterstitialAd mInterstitial1, mInterstitial2, mInsterstitialHome;
    private com.google.android.gms.ads.AdListener mInsterstitialListener;

    private static AdFacebook sInstance;

    public static AdFacebook getAdInstance(Activity activity){
        if(sInstance == null)
            sInstance = new AdFacebook(activity);
        return sInstance;
    }

    public static boolean isAdLoad(View view){
        if(view.getTag() != null && view.getTag() instanceof NativeAd && ((NativeAd) view.getTag()).isAdLoaded())
            return true;
        return false;
    }


    public AdFacebook(Activity activity){
        mActivity = activity;
//        mInterstitial1 = new InterstitialAd(mActivity, "274843063039758_294696277721103");
//        mInterstitial2 = new InterstitialAd(mActivity, "274843063039758_294697551054309");
//        mInsterstitialHome = new InterstitialAd(mActivity, "274843063039758_294696114387786");
//        mInsterstitialHome.loadAd();
//        loadInsterstitial();
    }

//    public void reloadDefaultCampaign(){
//        Gson gson = new Gson();
//        if(PrefUtil.getStringPreference(mActivity, Constants.Pref.BASIC_INFO_DATA) != null) {
//            BasicInfoData info = gson.fromJson(PrefUtil.getStringPreference(mActivity, Constants.Pref.BASIC_INFO_DATA), BasicInfoData.class);
//            mCampaignList = info.getCampaign();
//        }
//    }
//
//    public CampaignData getRandomCampaignData(){
//        if(mCampaignList != null && mCampaignList.size() > 0)
//            return mCampaignList.get((int)(Math.random() * mCampaignList.size()));
//        return null;
//    }

    public void loadInsterstitial(){
//        if(!mInterstitial1.isAdLoaded()) {
//            mInterstitial1.loadAd();
//            mInterstitial1.setAdListener(new InterstitialAdListener() {
//                @Override
//                public void onInterstitialDisplayed(Ad ad) {
//                    GameAudioManager.getInstance(mActivity).setInsterstialShow(true);
//                    // Interstitial displayed callback
//                }
//
//                @Override
//                public void onInterstitialDismissed(Ad ad) {
//                    GameAudioManager.getInstance(mActivity).setInsterstialShow(false);
//                    if(mInsterstitialListener != null)
//                        mInsterstitialListener.onAdClosed();
//                    loadInsterstitial();
//                }
//
//                @Override
//                public void onError(Ad ad, AdError adError) {
//                }
//
//                @Override
//                public void onAdLoaded(Ad ad) {
//                }
//
//                @Override
//                public void onAdClicked(Ad ad) {
//                    // Ad clicked callback
//                }
//
//                @Override
//                public void onLoggingImpression(Ad ad) {
//                    // Ad impression logged callback
//                }
//            });
//        }
//        if(!mInterstitial2.isAdLoaded()) {
//            mInterstitial2.loadAd();
//            mInterstitial2.setAdListener(new InterstitialAdListener() {
//                @Override
//                public void onInterstitialDisplayed(Ad ad) {
//                    GameAudioManager.getInstance(mActivity).setInsterstialShow(true);
//                    // Interstitial displayed callback
//                }
//
//                @Override
//                public void onInterstitialDismissed(Ad ad) {
//                    GameAudioManager.getInstance(mActivity).setInsterstialShow(false);
//                    if(mInsterstitialListener != null)
//                        mInsterstitialListener.onAdClosed();
//                    loadInsterstitial();
//                }
//
//                @Override
//                public void onError(Ad ad, AdError adError) {
//                }
//
//                @Override
//                public void onAdLoaded(Ad ad) {
//                }
//
//                @Override
//                public void onAdClicked(Ad ad) {
//                    // Ad clicked callback
//                }
//
//                @Override
//                public void onLoggingImpression(Ad ad) {
//                    // Ad impression logged callback
//                }
//            });
//        }
    }

    public void showHomeInsterstial(){
//        if(mInsterstitialHome != null) {
//            mInsterstitialHome.setAdListener(new InterstitialAdListener() {
//                @Override
//                public void onInterstitialDisplayed(Ad ad) {
//                    GameAudioManager.getInstance(mActivity).setInsterstialShow(true);
//                    // Interstitial displayed callback
//                }
//
//                @Override
//                public void onInterstitialDismissed(Ad ad) {
//                    GameAudioManager.getInstance(mActivity).setInsterstialShow(false);
//                    if(mInsterstitialListener != null)
//                        mInsterstitialListener.onAdClosed();
//                }
//
//                @Override
//                public void onError(Ad ad, AdError adError) {
//                }
//
//                @Override
//                public void onAdLoaded(Ad ad) {
//                }
//
//                @Override
//                public void onAdClicked(Ad ad) {
//                    // Ad clicked callback
//                }
//
//                @Override
//                public void onLoggingImpression(Ad ad) {
//                    // Ad impression logged callback
//                }
//            });
//            CountDownTimer countDownTimer = new CountDownTimer(6000, 1000) {
//                @Override
//                public void onTick(long l) {
//                    if (mInsterstitialHome != null && mInsterstitialHome.isAdLoaded())
//                        this.onFinish();
//                }
//
//                @Override
//                public void onFinish() {
//                    if(mInsterstitialHome != null && mInsterstitialHome.isAdLoaded()) {
//                        mInsterstitialHome.show();
//                    }
//                }
//            };
//            countDownTimer.start();
//        }
    }

    public void showFailedAd(com.google.android.gms.ads.AdListener listener){
//        mInsterstitialListener = listener;
//        if(mInterstitial1 != null && mInterstitial1.isAdLoaded())
//            mInterstitial1.show();
//        else {
//            if(mInterstitial2 != null && mInterstitial2.isAdLoaded())
//                mInterstitial2.show();
//            else
//                loadInsterstitial();
//        }
    }

    public NativeAd loadNativeAd(final View viewParent, final String placeId, final AdListener listener){
        final NativeAd nativeAd = new NativeAd(mActivity, placeId);
        viewParent.setTag(nativeAd);
        nativeAd.setAdListener(new AdListener() {

            @Override
            public void onError(Ad ad, AdError error) {
                // Ad error callback
                if(listener != null)
                    listener.onError(ad, error);

                if(viewParent != null && viewParent.getLayoutParams() != null)
                    viewParent.getLayoutParams().height = 1;
                Log.e(TAG, "AdError:" + error.getErrorCode() + ", " + error.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd != null) {
                    nativeAd.unregisterView();
                }
                // Ad loaded callback
                if(viewParent == null) {
                    ad.destroy();
                    return;
                }
                setAdViewContent(nativeAd, viewParent);

                if(listener != null)
                    listener.onAdLoaded(ad);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                if(listener != null)
                    listener.onAdClicked(ad);
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        });

        // Request an ad
        nativeAd.loadAd();
        return nativeAd;
    }

    public void setAdViewContent(NativeAd nativeAd, View viewParent){
        viewParent.setTag(nativeAd);
        // Download and display the ad icon.
        ImageView nativeAdIcon = viewParent.findViewById(R.id.item_ad_fb_icon);
//                int padding = (int)(nativeAdIcon.getLayoutParams().width * 0.1f);
//                nativeAdIcon.setPadding(padding, padding, padding, padding);
        NativeAd.Image adIcon = nativeAd.getAdIcon();
        NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);


        MediaView mediaView = viewParent.findViewById(R.id.item_ad_media);
        if(mediaView != null) {
            mediaView.setBackgroundColor(Color.BLACK);
            mediaView.setNativeAd(nativeAd);
        }

        RelativeLayout adChoicesContainer = viewParent.findViewById(R.id.item_ad_choice_container);
        AdChoicesView adChoicesView = new AdChoicesView(mActivity, nativeAd, true);
        adChoicesContainer.addView(adChoicesView);

        TextView action = viewParent.findViewById(R.id.item_ad_fb_action);
        if(action != null)
            action.setText(nativeAd.getAdCallToAction());

        TextView content = viewParent.findViewById(R.id.item_ad_fb_desacription);
        if(content != null)
            content.setText(nativeAd.getAdSubtitle());

        TextView name = viewParent.findViewById(R.id.item_ad_fb_name);
        if(name != null)
            name.setText(nativeAd.getAdTitle());

        List<View> clickableViews = new ArrayList<>();
        if(action != null)
            clickableViews.add(action);

        if(mediaView != null)
            clickableViews.add(mediaView);

        nativeAd.registerViewForInteraction(viewParent, clickableViews);
    }
//    public void loadNativeAd(View layout, int parentHeight, String placeId) {
//        loadNativeAd(layout, parentHeight, placeId, null);
//    }

}
