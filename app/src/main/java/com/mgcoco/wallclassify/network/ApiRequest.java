package com.mgcoco.wallclassify.network;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.mgcoco.wallclassify.entity.response.BaseModel;
import com.mgcoco.wallclassify.entity.response.BasicInfoData;
import com.mgcoco.wallclassify.entity.response.ClassesModel;
import com.mgcoco.wallclassify.entity.response.LoginData;
import com.mgcoco.wallclassify.entity.response.NoticeData;
import com.mgcoco.wallclassify.util.CommonUtil;
import com.mgcoco.wallclassify.util.Constants;
import com.mgcoco.wallclassify.util.PrefUtil;
import com.mgcoco.wallclassify.view.activity.BaseActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by softicecan on 2017/12/25.
 */

public class ApiRequest {

    public final static int REQUEST_STATUS_FAILED = 0;
    public final static int REQUEST_STATUS_SUCCESS = 1;
    public final static int REQUEST_STATUS_INVALID_TOKEN = 401;
    private static ApiRequest sInstance;

    public static ApiRequest getInstance(){
        if(sInstance == null)
            sInstance = new ApiRequest();
        return sInstance;
    }

    public void addDeviceToken(final Context context, String token, final ApiCallbackListener listener){
        if(context == null)
            return;
        Map<String, List<String>> params = new HashMap<>();
        params.put("deviceToken", new ArrayList<>(Arrays.asList(token)));
        params.put("device", new ArrayList<>(Arrays.asList("android")));
        params.put("udid", new ArrayList<>(Arrays.asList(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID))));

        Builders.Any.B b = startApiRequest(context, "fcm/addDeviceToken", params, true);
        b.as(new TypeToken<BaseModel<BaseModel>>(){}).setCallback(new FutureCallback<BaseModel<BaseModel>>() {
            @Override
            public void onCompleted(Exception e, BaseModel<BaseModel> result) {
                handleResponse(context, true, e, result, listener);
            }
        });;
    }

    public void getBasicInfo(final Context context, final ApiCallbackListener listener){
        if(context == null)
            return;
        Builders.Any.B b = startApiRequest(context, "basic/getBasicInfo", null, true);
        b.as(new TypeToken<BaseModel<BasicInfoData>>(){}).setCallback(new FutureCallback<BaseModel<BasicInfoData>>() {
            @Override
            public void onCompleted(Exception e, BaseModel<BasicInfoData> result) {
                handleResponse(context, true, e, result, listener);
            }
        });
    }

    public void login(final Context context, final ApiCallbackListener listener){
        if(context == null)
            return;
        Map<String, List<String>> params = new HashMap<>();
        params.put("id", new ArrayList<>(Arrays.asList( AccessToken.getCurrentAccessToken().getUserId())));
        params.put("accessToken", new ArrayList<>(Arrays.asList(AccessToken.getCurrentAccessToken().getToken())));
        params.put("device", new ArrayList<>(Arrays.asList("android")));
        params.put("udid", new ArrayList<>(Arrays.asList(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID))));
        params.put("version", new ArrayList<>(Arrays.asList(CommonUtil.getVersionCode(context))));
        Builders.Any.B b = startApiRequest(context, "user/login", params, true);
        b.as(new TypeToken<BaseModel<LoginData>>(){}).setCallback(new FutureCallback<BaseModel<LoginData>>() {
            @Override
            public void onCompleted(Exception e, BaseModel<LoginData> result) {
                handleResponse(context, true, e, result, listener);
            }
        });
    }

    public void getAllClasses(final Context context, final ApiCallbackListener listener){
        if(context == null)
            return;
        Map<String, List<String>> params = new HashMap<>();
        Builders.Any.B b = startApiRequest(context, "wall/getAllClasses", null, true);
        b.as(new TypeToken<BaseModel<ClassesModel>>(){}).setCallback(new FutureCallback<BaseModel<ClassesModel>>() {
            @Override
            public void onCompleted(Exception e, BaseModel<ClassesModel> result) {
                handleResponse(context, true, e, result, listener);
            }
        });
    }

//    public void completeGame(final Context context, final GameInfo gameInfo, final ApiCallbackListener listener){
//        if(context == null)
//            return;
//        Map<String, List<String>> params = new HashMap<>();
//        params.put("type", new ArrayList<>(Arrays.asList(gameInfo.getType())));
//        params.put("level", new ArrayList<>(Arrays.asList(gameInfo.getLevel())));
//        params.put("completion", new ArrayList<>(Arrays.asList(gameInfo.isCompletion())));
//        params.put("failureTimes", new ArrayList<>(Arrays.asList(gameInfo.getFailureTimes())));
//        params.put("maze", new ArrayList<>(Arrays.asList(gameInfo.getMaze())));
//        params.put("startTime", new ArrayList<>(Arrays.asList(gameInfo.getStartTime())));
//        params.put("endTime", new ArrayList<>(Arrays.asList(gameInfo.getEndTime())));
//        params.put("duration", new ArrayList<>(Arrays.asList(gameInfo.getDuration())));
//        Builders.Any.B b = startApiRequest(context, "lottory/completeGame", params, true);
//        b.as(new TypeToken<BaseModel<CompleteGameData>>(){}).setCallback(new FutureCallback<BaseModel<CompleteGameData>>() {
//            @Override
//            public void onCompleted(Exception e, BaseModel<CompleteGameData> result) {
//                handleResponse(context, true, e, result, listener);
//            }
//        });
//    }

    public void getUserInfo(final Context context, final ApiCallbackListener<LoginData> listener){
        if(context == null)
            return;
        Builders.Any.B b = startApiRequest(context, "user/getUserInfo", null, true);
        b.as(new TypeToken<BaseModel<LoginData>>(){}).setCallback(new FutureCallback<BaseModel<LoginData>>() {
            @Override
            public void onCompleted(Exception e, BaseModel<LoginData> result) {
                handleResponse(context, true, e, result, listener);
            }
        });
    }

//    public void setRecommanded(final Context context, final String recommandedId, final ApiCallbackListener<LoginData> listener){
//        if(context == null)
//            return;
//
//        Map<String, List<String>> params = new HashMap<>();
//        params.put("recommandedId", new ArrayList<>(Arrays.asList(recommandedId)));
//        Builders.Any.B b = startApiRequest(context, "user/setRecommanded", params, true);
//        b.as(new TypeToken<BaseModel<LoginData>>(){}).setCallback(new FutureCallback<BaseModel<LoginData>>() {
//            @Override
//            public void onCompleted(Exception e, BaseModel<LoginData> result) {
//                handleResponse(context, true, e, result, listener);
//            }
//        });
//    }
//
//    public void getRankList(final Context context, final ApiCallbackListener<RankData> listener){
//        if(context == null)
//            return;
//        Builders.Any.B b = startApiRequest(context, "lottory/getRank", null, true);
//        b.as(new TypeToken<BaseModel<RankData>>(){}).setCallback(new FutureCallback<BaseModel<RankData>>() {
//            @Override
//            public void onCompleted(Exception e, BaseModel<RankData> result) {
//               handleResponse(context, true, e, result, listener);
//            }
//        });
//    }

    public void getNoticeList(final Context context, final ApiCallbackListener<NoticeData> listener){
        if(context == null)
            return;
        Builders.Any.B b = startApiRequest(context, "notice/getNoticeList", null, true);
        b.as(new TypeToken<BaseModel<NoticeData>>(){}).setCallback(new FutureCallback<BaseModel<NoticeData>>() {
            @Override
            public void onCompleted(Exception e, BaseModel<NoticeData> result) {
                handleResponse(context, true, e, result, listener);
            }
        });
    }


    private Builders.Any.B startApiRequest(final Context context, String url, Map<String, List<String>> param, final boolean isShowLoading){
        if(context == null)
            return null;
        if(context instanceof BaseActivity && isShowLoading)
            ((BaseActivity)context).showLoadingView();


        Builders.Any.B b = Ion.with(context).load(Constants.DOMAIN_URL + url);
        if(param != null)
            b.setBodyParameters(param);
        if(AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().getToken() != null)
            b.setHeader("token", AccessToken.getCurrentAccessToken().getToken());
        return b;
    }

    private void handleResponse(final Context context, boolean isShowLoading, Exception e, Object result, ApiCallbackListener listener){
        if(context instanceof BaseActivity && isShowLoading)
            ((BaseActivity)context).removeApiRequest();

        if(e != null) {
            Log.e("", "handleResponse error:" + e);
//            listener.onFailure();
        }
        else {
            Gson g = new Gson();
            String re = g.toJson(result);
            Log.e("", "handleResponse success:" + re);
            if(((BaseModel)result).getStatus() == REQUEST_STATUS_INVALID_TOKEN) {
                LoginManager.getInstance().logOut();
                PrefUtil.setBooleanPreference(context, Constants.Pref.IS_AGREE_TERMS, false);
                listener.onFailure(((BaseModel) result).getStatus(), ((BaseModel) result).getMessage());
                return;
            }
            if(((BaseModel)result).getStatus() != REQUEST_STATUS_SUCCESS) {
                Toast.makeText(context, ((BaseModel) result).getMessage(), Toast.LENGTH_SHORT).show();
                listener.onFailure(((BaseModel) result).getStatus(), ((BaseModel) result).getMessage());
            }
            else
                listener.onSuccess(((BaseModel)result).getData());
        }
    }

}
