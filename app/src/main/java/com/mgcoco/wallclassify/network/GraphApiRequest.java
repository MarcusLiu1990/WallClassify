package com.mgcoco.wallclassify.network;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mgcoco.wallclassify.entity.response.LikeData;
import com.mgcoco.wallclassify.entity.response.PageData;
import com.mgcoco.wallclassify.entity.response.PostData;
import com.mgcoco.wallclassify.view.activity.BaseActivity;

import org.json.JSONException;

import java.util.List;

/**
 * Created by softicecan on 2018/3/4.
 */

public class GraphApiRequest {

    private static GraphApiRequest sInstance;

    public static GraphApiRequest getInstance(){
        if(sInstance == null)
            sInstance = new GraphApiRequest();
        return sInstance;
    }

    public GraphRequestAsyncTask getPosts(Context context, String id, boolean isShowLoading, final ApiCallbackListener listener){
        return startRequest(context, id, isShowLoading, "posts.limit(10){link,message,full_picture,place,created_time,type,properties},name,id", new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                if(response.getError() == null) {
                    try {
                        Log.e("", "data:" + response.getJSONObject().getJSONObject("posts").getString("data"));
                        String name = response.getJSONObject().getString("name");
                        String pageId = response.getJSONObject().getString("id");
                        List<PostData> list = new Gson().fromJson(response.getJSONObject().getJSONObject("posts").getString("data"), new TypeToken<List<PostData>>(){}.getType());
                        for(int i = 0; i < list.size(); i++){
                            list.get(i).setPageId(pageId);
                            list.get(i).setName(name);
                        }
                        listener.onSuccess(list);
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }

                }
                else {
                    listener.onFailure(response.getError().getErrorCode(), response.getError().getErrorMessage());
                    Log.e("", "data error:" + response.getError().getErrorCode() + ", " + response.getError().getErrorMessage());
                }
            }
        });
    }


    public GraphRequestAsyncTask getLikes(Context context, boolean isShowLoading, final ApiCallbackListener listener){
        return startRequest(context, "me/likes", isShowLoading, "id,name,picture,created_time", new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                if(response.getError() == null) {
                    try {
                        Log.e("", "data:" + response.getJSONObject().toString());
                        listener.onSuccess(new Gson().fromJson(response.getJSONObject().getString("data"), new TypeToken<List<LikeData>>(){}.getType()));
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }

                }
                else {
                    listener.onFailure(response.getError().getErrorCode(), response.getError().getErrorMessage());
                    Log.e("", "data error:" + response.getError().getErrorCode() + ", " + response.getError().getErrorMessage());
                }
            }
        });
    }

    public void getPageInfo(Context context, String id, final ApiCallbackListener listener){
//        GraphRequest request = GraphRequest.newGraphPathRequest(
//                AccessToken.getCurrentAccessToken(),
//                "/" + id,
//                new GraphRequest.Callback() {
//                    @Override
//                    public void onCompleted(GraphResponse response) {
//                        if(response.getError() == null)
//                            listener.onSuccess(new Gson().fromJson(response.getJSONObject().toString(), listener.getType().getClass()));
//                        else
//                            listener.onFailure(response.getError().getErrorCode(), response.getError().getErrorMessage());
//                    }
//                });
//
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "posts.limit(10){link,message,full_picture,place,created_time}");
//        request.setParameters(parameters);
//        request.executeAsync();

//        startRequest(context, id, "posts.limit(10){link,message,full_picture,place,created_time,type,properties}",  new GraphRequest.Callback() {
//            @Override
//            public void onCompleted(GraphResponse response) {
//                if(response.getError() == null) {
//                    try {
//                        listener.onSuccess(new Gson().fromJson(response.getJSONObject().getString("data"), PageData.class));
//                    }
//                    catch (JSONException e){
//
//                    }
//                }
//                else
//                    listener.onFailure(response.getError().getErrorCode(), response.getError().getErrorMessage());
//            }
//        });
    }

    public void findFacebookId(Context context, String url, boolean isShowLoading, final ApiCallbackListener listener){
        startRequest(context, "?id=" + url, isShowLoading, null,  new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                if(response.getError() == null) {
                    listener.onSuccess(new Gson().fromJson(response.getJSONObject().toString(), PageData.class));
                }
                else
                    listener.onFailure(response.getError().getErrorCode(), response.getError().getErrorMessage());
            }
        });
    }

    private GraphRequestAsyncTask startRequest(final Context context, String graphPath, final boolean isShowLoading, String fields, final GraphRequest.Callback listener){
        if(context != null && isShowLoading){
            ((BaseActivity)context).showLoadingView();
        }

        GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + graphPath, new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if(context != null && isShowLoading){
                            ((BaseActivity)context).removeApiRequest();
                        }
                        listener.onCompleted(response);
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", fields);
        request.setParameters(parameters);
        return request.executeAsync();
    }

}
