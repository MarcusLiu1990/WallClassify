package com.mgcoco.wallclassify.network;

/**
 * Created by softicecan on 2017/12/25.
 */

public interface ApiCallbackListener<T> {
    void onSuccess(T data);

    void onFailure(int status, String message);
}
