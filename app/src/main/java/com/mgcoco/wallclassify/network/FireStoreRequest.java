package com.mgcoco.wallclassify.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.mgcoco.wallclassify.entity.response.ClassifyData;
import com.mgcoco.wallclassify.view.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by softicecan on 2018/3/13.
 */

public class FireStoreRequest {

    private static final String TAG = FireStoreRequest.class.getSimpleName();

    private static final ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(2, 4,
            60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final FirebaseFirestore mDb;


    public FireStoreRequest(FirebaseFirestore db) {
        this.mDb = db;
    }

    public void getAllClassification(final Context context, final ApiCallbackListener<List<ClassifyData>> callbackListener) {
        if(context instanceof MainActivity)
            ((MainActivity)context).showLoadingView();

        mDb.collection("Class")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(context instanceof MainActivity)
                            ((MainActivity)context).removeApiRequest();

                        if (task.isSuccessful()) {
                            List<ClassifyData> list = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, "fireStore:: " + document.getId() + " => " + document.toObject(ClassifyData.class));
                                ClassifyData data = document.toObject(ClassifyData.class);
                                data.set_id(document.getId());
                                list.add(data);
                            }
                            List<ClassifyData> orderList = new ArrayList<>();
                            for(int i = 0; i < list.size(); i++){
                                for(int j = 0; j < list.size(); j++) {
                                    if (list.get(j).getOrder() == i) {
                                        orderList.add(list.get(j));
                                        break;
                                    }
                                }
                            }

                            callbackListener.onSuccess(orderList);
                        }
                        else {
                            Log.e(TAG, "fireStore:: Error getting documents." +  task.getException());
                        }
                    }
                });
    }
}
