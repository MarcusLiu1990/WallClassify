package com.mgcoco.wallclassify;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;

/**
 * Created by softicecan on 2018/5/5.
 */

public class WallClassifyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        Fresco.initialize(this);
    }
}
